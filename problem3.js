const dataSet = require("./js_drill_5.js");

//From the given data, filter all the unique langauges and return in an array

function getUniqueLanguage(dataSet) {
  if (Array.isArray(dataSet)) {
    newObject = {};
    uniqueLanguages = [];
    dataSet.forEach(function (data) {
      data.languages.forEach((element) => {
        if (!uniqueLanguages.includes(element)) {
          uniqueLanguages.push(element);
        }
      });
    });
    return uniqueLanguages;
  } else {
    return null;
  }
}
module.exports=getUniqueLanguage;



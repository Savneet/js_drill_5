const dataSet = require("./js_drill_5.js");

// Check the each person projects list and group the data based on the project status and return the data as below

/*
  
    {
      'Completed': ["Mobile App Redesign","Product Launch Campaign","Logo Redesign","Project A".. so on],
      'Ongoing': ["Website User Testing","Brand Awareness Campaign","Website Mockup" .. so on]
      .. so on
    }
  
  */

function projectStatusProblem(dataSet) {
  if (Array.isArray(dataSet)) {
    let newObject = {};
    dataSet.forEach(function (data) {
      data.projects.forEach(function (projectData) {
        if (projectData.status in newObject) {
          newObject[projectData.status].push(projectData.name);
        } else {
          newObject[projectData.status] = [projectData.name];
        }
      });
    });
    return newObject;
  } else {
    return null;
  }
}
module.exports = projectStatusProblem;

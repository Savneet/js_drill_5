const dataSet = require("./js_drill_5.js");

//Iterate over each object in the array and in the email breakdown the email and return the output as below:
/*
     Note: use only email property to get the below details
  
     [
        {
        firstName: 'John', // the first character should be in Uppercase
        lastName: 'Doe', // the first character should be in Uppercase
        emailDomain: 'example.com'
        },
        {
        firstName: 'Jane',
        lastName: 'Smith',
        emailDomain: 'gmail.com'
        }
        .. so on
     ]
  */

function getDetails(dataSet) {
  if (Array.isArray(dataSet)) {
    let newData = dataSet.map(function (data) {
      let splitName = data.name.split(" ");
      let splitEmail = data.email.split("@");
      let firstName = splitName[0][0].toUpperCase() + splitName[0].slice(1); // Converting first char to upper case and rest of string is concatenated from index 1 to end
      let lastName = splitName[1][0].toUpperCase() + splitName[1].slice(1);
      let emailDomain = splitEmail[1];

      let outputData = {
        firstName: firstName,
        lastName: lastName,
        emailDomain: emailDomain,
      };

      return outputData;
    });

    return newData;
  } else {
    return null;
  }
  
}

module.exports=getDetails;
